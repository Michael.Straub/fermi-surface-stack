from mayavi import mlab
import numpy as np
import copy

def main(FS_dict, e_arr, z_axis = None, axis = True, beQuiet = True, vmin = 0, vmax = None, colormap = None, sliceIntegration = 0):
    """
    The main function starts a mayavi mlab application
    :param FS_dict:
    :param e_arr: numpy array of the energies to be stacked.
    :param z_axis: numpy array for the z_positions in space for the different energies.
    :param axis: if True an axis with ticks is drawn
    :param beQuiet: if True some additional info to debug is printed
    :param sliceIntegration: if energy should be intergrated choose non zero value
    :return: 0
    """



    if vmin is None:
        vmin = 0
    if vmax is None:
        vmax = np.max(FS_dict['data'])

    #create in-plane axis
    y_arr = FS_dict['Axis'][1]
    x_arr = FS_dict['Axis'][2]

    if z_axis is None:
        z_axis = 3 * np.linspace(np.min(x_arr) , np.max(x_arr), len(e_arr))

    X, Y = np.meshgrid(x_arr, y_arr)


    fig = mlab.figure(1, bgcolor=(1, 1, 1), size=(300, 600))


    #define vertices of the big cube:
    if axis is True:
        vertices = [(x_arr[0], y_arr[0], e_arr[0]), (x_arr[-1], y_arr[0], e_arr[0]), (x_arr[-1], y_arr[-1], e_arr[0]),
                    (x_arr[0], y_arr[-1], e_arr[0]),(x_arr[0], y_arr[0], e_arr[-1]), (x_arr[-1], y_arr[0], e_arr[-1]),
                    (x_arr[-1], y_arr[-1], e_arr[-1] * 5), (x_arr[0], y_arr[-1], e_arr[-1])]
        #define axes edges:
        axis_edges = [(1, 2), (2, 3), (1, 5)]
        #draw axes
        for edge in axis_edges:
            x_pts = [vertices[edge[0]][0], vertices[edge[1]][0]]
            y_pts = [vertices[edge[0]][1], vertices[edge[1]][1]]
            z_pts = [vertices[edge[0]][2] , vertices[edge[1]][2]]
            mlab.plot3d(x_pts, y_pts, z_pts, color=(0, 0, 0), tube_radius=0.01)

        for tick in [-1, 0, 1]:
            x_pts = np.ones(2) * tick
            y_pts = np.array([y_arr[-1], y_arr[-1] + 0.2])
            z_pts = np.ones(2) * z_axis[0]
            mlab.plot3d(x_pts, y_pts, z_pts, color=(0, 0, 0), tube_radius=0.002)
            # mlab.text3d(tick,y_arr[-1] +0.2,e_arr[0]* e_scale, str(tick), scale = 0.1 ,color = (0,0,0),line_width = 0.1)
            x_pts = np.array([x_arr[-1], x_arr[-1] + 0.2])
            y_pts = np.ones(2) * tick
            z_pts = np.ones(2) * z_axis[0]
            mlab.plot3d(x_pts, y_pts, z_pts, color=(0, 0, 0), tube_radius=0.002)

    for i, energy in enumerate(e_arr):
        if axis is True:
            vertices = [(x_arr[0], y_arr[0], z_axis[i]), (x_arr[-1], y_arr[0], z_axis[i]), (x_arr[-1], y_arr[-1], z_axis[i]),
                        (x_arr[0], y_arr[-1], z_axis[i])]
            edges = [(0, 1), (1, 2), (2, 3), (3, 0)]
            # border for individual surface
            for edge in edges:
                x_pts = [vertices[edge[0]][0], vertices[edge[1]][0]]
                y_pts = [vertices[edge[0]][1], vertices[edge[1]][1]]
                z_pts = [vertices[edge[0]][2], vertices[edge[1]][2]]
                mlab.plot3d(x_pts, y_pts, z_pts, color=(0.3, 0.3, 0.3), tube_radius=0.002)
            tick_edge = edges[1]
            x_pts = [vertices[tick_edge[0]][0], vertices[tick_edge[1]][0]]
            y_pts = [vertices[tick_edge[0]][1] - 0.2, vertices[tick_edge[0]][1]]
            z_pts = [vertices[tick_edge[0]][2], vertices[tick_edge[1]][2]]
            mlab.plot3d(x_pts, y_pts, z_pts, color=(0, 0, 0), tube_radius=0.002)

        eSurface = getSlice(FS_dict, 0, energy, sliceIntegration=sliceIntegration)


        im = mlab.imshow(eSurface['data'].T,
                     extent=[x_arr[0], x_arr[-1], y_arr[0], y_arr[-1], z_axis[i], z_axis[i]],
                     colormap=colormap, figure=fig, opacity=1.0, vmin=0, vmax=vmax)

    mlab.gcf().scene.parallel_projection = True
    mlab.view(elevation=65)

    mlab.show()

def getSlice(spectrum, axis, axisValue, sliceIntegration=0, normalized=False, beQuiet=True):
    def array_slice(a, axis, start, end, step=1):
        if start == end: end = start + 1

        return a[(slice(None),) * (axis % a.ndim) + (slice(start, end, step),)]

    outputSpectrum = copy.deepcopy(spectrum)

    a = np.asarray(outputSpectrum['Axis'][axis])
    if sliceIntegration == 0:
		frameIndex = (np.abs(a - axisValue)).argmin()
		image = array_slice(spectrum['data'], axis=axis, start=frameIndex, end=frameIndex)
		image = np.sum(image, axis=axis)

	else:
		value = axisValue - (sliceIntegration / 2)
		startFrameIndex = (np.abs(a - value)).argmin()

		value = axisValue + (sliceIntegration / 2)
		endFrameIndex = (np.abs(a - value)).argmin()

		if startFrameIndex > endFrameIndex: startFrameIndex, endFrameIndex = endFrameIndex, startFrameIndex

		if beQuiet == False:
			print("Requested axisValue {} along axis {} ('{}')".format(axisValue, axis, spectrum['AxisLabel'][axis]))
			print("That axis spans {:.4f} ... {:.4f} and contains {} points".format(a[0], a[-1], len(a)))
			if startFrameIndex == endFrameIndex and sliceIntegration != 0:
				print("Requested integration is too small, only returning a single frame")
			if startFrameIndex == endFrameIndex:
				print("Extracting frame index {} ({:.4f}) of axis '{}'".format(startFrameIndex, a[startFrameIndex],
																			   spectrum['AxisLabel'][axis]))
			else:
				print("An integration of {} {} means summing over indices {} through {}".format(sliceIntegration,
																								spectrum['AxisUnits'][
																									axis],
																								startFrameIndex,
																								endFrameIndex))

		image = array_slice(spectrum['data'], axis=axis, start=startFrameIndex, end=endFrameIndex)

	    image = np.sum(image, axis=axis)

    if normalized == True:
        image = image / (1 + endFrameIndex - startFrameIndex)

    outputSpectrum['data'] = image

    axesList = outputSpectrum['Axis']
    if type(axesList) is not list:
        axesList = [axis for axis in axesList]
    axesList.pop(axis)
    outputSpectrum['Axis'] = axesList

    return outputSpectrum

if __name__ == "__main__":
    import pickle

    with open('FS_example.pkl', 'rb') as fp:
        FS_example = pickle.load(fp)

    e_arr = np.array([88.62, 88.77, 88.92, 89.07, 89.22])

    main(FS_example,e_arr, z_axis= 10 * e_arr, axis = True, colormap= 'Purples')
