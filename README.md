# Fermi surface stack

This is a small mayavi application written in python (mlab) to create a Fermi surface stack.
In order for this to work, the data has to be in a dictionary comparable to the example data provided with a 3d data cube with dimensions (energy, k_axis1, k_axis2)

## Installation:
The only non-standard package is mayavi's mlab.
Also it is a qt-application so IPython or sth. similar is needed for the interactive view.

Easiest way to install both is pip:
```
$ pip install mayavi

$ pip install PyQt5
```

## Authors
This was created by Michael Straub and is currently not maintained.